const mongoose = require('mongoose');

const BankHistotySchema = mongoose.Schema({
    transactionName: {type: String, required: true},
    user_id: {type: String, required: true},
    ad_id: {type: String},
    adCategories: {type: String},
    adSubCategories: {type: String},
    adType: {type: String},
    adPrice: {type: String},
    to: {type: String},
    amount: {type: String},
    transactionStatus: {type: String, required: true},
    transactionDate: {type: String, required: true}
});


module.exports = mongoose.model('BankHistory', BankHistotySchema);