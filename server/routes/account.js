const express = require('express');
const router = express.Router();
const _ = require('lodash');
const AccountSchema = require('../db/account');
const timestamp = require('time-stamp');
const BankHistorySchema = require('../db/history');

router.get('/check-balance/:userId', (req, res, next) => {
    var user_id = req.params.userId;

    AccountSchema.checkAccount(user_id)
        .then(balance => {
            res.json(balance);
        });
});

router.post('/', (req, res, next) => {

    var body = _.pick(req.body, ['user_id']);

    body.arsabi_credit = 20;

    var account = new AccountSchema(body);
    account.save()
        .then(account => {
            res.status(200)
                .json(account);
        })
        .catch(err => {
            res.status(401)
                .send(err);
        });
});

router.patch('/make-payment/:user_id', (req, res, next) => {

    var user_id = req.params.user_id;
    var body = _.pick(req.body, ['user_id', 'adCategories', 'adSubCategories', 'adPrice', 'adType', 'adId']);

    AccountSchema.checkAccount(user_id)
        .then(balance => {

            var credit = balance.arsabi_credit;
            var price = body.adPrice;
            var arsabi_credit = credit - price;

            AccountSchema.findOneAndUpdate({
                user_id
            }, {
                $set: {
                    arsabi_credit
                }
            }, {
                new: true
            }).then(account => {

                body.adPrice = price + " Arsabi Credit";
            
                var today = new Date();
                body.transactionDate = timestamp('DD-MM-YYYY', today);
                body.transactionStatus = "On Hold";
                body.transactionName = "Place Ad";

                Transaction(res, body);

            }).catch(err => {
                res.status(400)
                    .send();
            });
        });
});

var Transaction = (res, body) => {
    var bank = new BankHistorySchema(body);
    bank.save()
        .then(data => {
            res.status(200)
                .json(data);
        });
}
module.exports = router;