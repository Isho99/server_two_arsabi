const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');

var app = express();
var port =  process.env.PORT || 4000;

mongoose.connect('mongodb://localhost/arsabiServerTwo');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(morgan('dev'))

const Account = require('./server/routes/account');
const BankHistory = require('./server/routes/history');

app.use('/account', Account);
app.use('/bank-history', BankHistory);

app.listen(port, console.log(`Server running on ${port}`));

module.exports.app = app;